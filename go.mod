module websockets

go 1.21

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/lib/pq v1.10.9 // indirect
)
