# Use the official Go image as the base image
FROM golang:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the Go project files to the container
COPY . .

# Copy the .env file to the container
COPY .env .

# Build the Go project
RUN go build -o main .

# Set the environment variables from the .env file
CMD ["./main"]