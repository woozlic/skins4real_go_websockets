package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"log"
	"net/url"
	"os"
	"strconv"
	"time"
)

type Message struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

type SalesData struct {
	Time           time.Time `json:"time"`
	ClassInstance  string    `json:"class_instance"`
	MarketHashName string    `json:"market_hash_name"`
	SoldPrice      float64   `json:"sold_price"`
	Currency       string    `json:"currency"`
}

func decodeJson(row []byte) SalesData {
	var message Message
	err := json.Unmarshal([]byte(row), &message)
	if err != nil {
		fmt.Println("Error parsing JSON:", err)
		return SalesData{}
	}

	// Unescape the data field to get the original JSON array
	unescapedData := ""
	err = json.Unmarshal([]byte(message.Data), &unescapedData)
	if err != nil {
		fmt.Println("Error unescaping data:", err)
		return SalesData{}
	}
	var (
		class            string
		instance         string
		marketHashName   string
		utcTimestamp     string
		strMoney         string
		money            float64
		ruMarketHashName string
		color            string
		currency         string
		itemID           string
	)

	err = json.Unmarshal([]byte(unescapedData), &[]interface{}{
		&class, &instance, &marketHashName, &utcTimestamp, &strMoney, &ruMarketHashName, &color, &currency, &itemID,
	})

	if err != nil {
		fmt.Println("Error parsing inner JSON:", err)
		return SalesData{}
	}

	money, err = strconv.ParseFloat(strMoney, 64)
	money /= 100.0
	classInstance := class + "_" + instance
	unixTimeInt, err := strconv.ParseInt(utcTimestamp, 10, 64)
	if err != nil {
		fmt.Println("Error parsing Unix timestamp:", err)
		return SalesData{}
	}

	// Create a time.Time instance from the Unix timestamp
	unixTime := time.Unix(unixTimeInt, 0)

	return SalesData{unixTime, classInstance, marketHashName, money, currency}
}

func wsHistory(conn *websocket.Conn, db *sql.DB) {
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Printf("Error reading message: %v", err)
			break
		}
		salesData := decodeJson(message)
		if salesData.Currency != "RUB" {
			// смотрим только рубли
			continue
		}
		_, err = db.Exec("INSERT INTO sales_statistics (time, class_instance, market_hash_name, sold_price, currency) VALUES ($1, $2, $3, $4, $5)",
			salesData.Time, salesData.ClassInstance, salesData.MarketHashName, salesData.SoldPrice, salesData.Currency)
		if err != nil {
			log.Printf("Error inserting data into PostgreSQL: %v", err)
			break
		}
		log.Printf("%v | %v | %v %v \n", salesData.ClassInstance,
			salesData.MarketHashName, salesData.SoldPrice, salesData.Currency)
	}
}

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("Error loading .env file")
		return
	}

	// Get the environment variables
	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbName := os.Getenv("DB_NAME")

	encodedPassword := url.QueryEscape(dbPassword)

	// Construct the database connection string
	dbURL := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", dbUsername, encodedPassword, dbHost, dbPort, dbName)

	for {
		db, err := sql.Open("postgres", dbURL)
		if err != nil {
			log.Printf("Error connecting to PostgreSQL: %v", err)
			time.Sleep(time.Second * 5)
			continue
		}
		defer db.Close()
		// WebSocket URL
		wsUrl := "wss://wsn.dota2.net/wsn/"

		// Create a WebSocket connection
		conn, _, err := websocket.DefaultDialer.Dial(wsUrl, nil)
		if err != nil {
			log.Printf("Error connecting to WebSocket: %v", err)
			time.Sleep(time.Second * 5)
			continue
		}
		defer conn.Close()

		// Select the "history_go" channel
		channel := "history_go"
		if err := conn.WriteMessage(websocket.TextMessage, []byte(channel)); err != nil {
			log.Printf("Error selecting channel: %v", err)
			continue
		}

		wsHistory(conn, db)
		
	}
}
